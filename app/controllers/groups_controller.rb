class GroupsController < ApplicationController
  def Groups
  end

  def index
    @groups = Groups.find(:all)
  end

  def new
    @group = Groups.new
  end

  def create
    @group = Groups.new(params[:groups])
    if @group.save
      redirect_to(@group)
    else
      render :action => 'new'
    end
  end

  def show
    @group = Groups.find(params[:id])
  end

  def destroy
    Groups.find(params[:id]).destroy
    redirect_to(groups_url)
  end

  def edit
    @group = Groups.find(params[:id])
  end

  def update
    @group = Groups.find(params[:id])
    if @group.update_attributes(params[:groups])
      flash[:notice] = 'All good ;)'
      redirect_to(@group)
    else
      flash[:notice] = 'All bad :('
    end
  end

end
