class Users < ActiveRecord::Base
  validates_uniqueness_of :login
  validates_presence_of :firstname
  validates_presence_of :lastname
  validates_length_of :score, :within => 1..5
end
