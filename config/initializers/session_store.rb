# Be sure to restart your server when you modify this file.

# Your secret key for verifying cookie session data integrity.
# If you change this key, all old sessions will become invalid!
# Make sure the secret is at least 30 characters and all random, 
# no regular words or you'll be exposed to dictionary attacks.
ActionController::Base.session = {
  :key         => '_StudentsAlpha_session',
  :secret      => '6534d413cbcc94cb50bcdfb42862a593db77a122435624ead98a6ca1cce4ac20108b9af02de301d2b5b327a078cfc9023887c829e9c1b1ca172b6f6e08b8c9f8'
}

# Use the database for sessions instead of the cookie-based default,
# which shouldn't be used to store highly confidential information
# (create the session table with "rake db:sessions:create")
# ActionController::Base.session_store = :active_record_store
